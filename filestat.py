"""
Simple text file an4lyzR.

Some time ago i ran across a challenge for beginner programmers to count the
letters in a text and thought to myself: "i can take that to the next level".
Here you see the result.

Features:
    Analyze one or more text files (V1.0)
    Count Characters and show them ordered by group. (V1.0)
    CMD Interface:
        Pass files via CMD. (v1.0)

Planned features:
    Extensive CMD interface:
        toggle some functions like machine-readable-mode, reduced output
        (show only letters that were actually found), silent mode., ignore
        certain Characters.
    Walk through entire directories and discover text files automatically.
    calculate averages and additional stats (max, min).
    show stats:
        per file,
        per folder,
        all
    Output to file (txt, md or json).


Maybe sometime:
    Curses UI
    Use as python module.
    Interactive mode (Enter text manually)

"""
import sys
import string
import math
import shutil
import locale
import argparse

from itertools import (takewhile, repeat)

DEBUG = False
# print stat only about chars that have actually been found
PRESERVE_SPACE = True


def rawbigcount(filename):
    """
    Count the lines in a file.

    see: http://stackoverflow.com/a/27517681
    """
    f = open(filename, 'rb')
    bufgen = takewhile(lambda x: x, (f.raw.read(1024 * 1024)
                                     for _ in repeat(None)))
    return sum(buf.count(b'\n') for buf in bufgen if buf)


def update_progress_inline(progress, total, msg):
    """
    Show msg along with inline progress_meter.

    Args:
        progress: The current iteration of whatever action is going on.
        total: The total amount of iterations to be done
        msg: The message after which the progress-meter is shown.
    """
    # Width of the terminal
    columns, _ = shutil.get_terminal_size()
    # length of the progress-meter
    # NOTE: this only the amount of chars within the brackets
    bar_width = 20
    # Total space available for the msg
    width = columns - bar_width - 8
    # Completion of the task in percent
    # Ensure it stops at 100%
    percent = min(math.floor(progress / total * 100), 100)

    sys.stdout.write("\r{msg:{width}}[{bar:{bar_width}}] {progress:>3}% ".format(
        msg=msg,
        # Ensure the bar never too long.
        # Happens sometimes when total < bar_width
        bar="=" * min(math.floor(progress * bar_width / total), bar_width),
        bar_width=bar_width,
        width=width,
        progress=percent
    ))
    # Flickering happens on some terminals without this
    sys.stdout.flush()


def count_file(filename, show_progress=False, use_result_var=None):
    """
    Count letters in the file.

    dict format:
        {
            "lower": {
                'a':0,
                'b':0,
                ...
            },
            "upper": {
                'A': 0,
                'B': 0,
                ...
            },
            "digits": {
                ...
            },
            "punctuation": {
                ...
            },
            "whitespace": {
                ...
            }
        }

       The keys expected are all chars in string.ascii_lowercase,
       string.ascii_uppercase (and so forth) respectively.
       See function body for example.

    Args:
        filename: Name of the file to count chars of
        show_progress: Whether inline progress-meter should be shown. disable
            for machine-readable output (NOT YET IMPLEMENTED)
        use_result_var: Dict to add the count to. A new one will
            be created if None. Caller has to make sure it corectly initialized

    Returns:
        Dictionary containing five sub dicts which contain the amount
            of chars found in each group. If use_result_var is not None the
            given dict will be used instead.

    """
    if use_result_var is not None:
        # Use supplied dict.
        result = use_result_var
    else:
        # Populate dicts with keys from string vars and zeros for value
        result = {}
        result["lower"] = {c: 0 for c in string.ascii_lowercase}
        result["upper"] = {c: 0 for c in string.ascii_uppercase}
        result["digits"] = {c: 0 for c in string.digits}
        result["whitespace"] = {c: 0 for c in string.whitespace}
        result["punctuation"] = {c: 0 for c in string.punctuation}

    # TODO: only do if progress is shown
    file_total_lines = rawbigcount(filename)

    def analyze_line(line):
        # Debug code is commented out below because this code is well
        # tested as is.
        # Just uncomment to use debug features here.

        # The loop below should be self explaining.
        # It just checks whether the char is in a group

        for char in line:
            # if DEBUG:
            #    print("Processing %5s," % repr(char), end='')
            if char in string.ascii_lowercase:
                # if DEBUG:
                #     print(" Group: Lowercase Letters")
                result["lower"][char] += 1
            elif char in string.ascii_uppercase:
                # if DEBUG:
                #    print(" Group: Uppercase Letters")
                result["upper"][char] += 1
            elif char in string.digits:
                # if DEBUG:
                #    print(" Group: Digits")
                result["digits"][char] += 1
            elif char in string.punctuation:
                # if DEBUG:
                #    print(" Group: Punctuation")
                result["punctuation"][char] += 1
            elif char in string.whitespace:
                # if DEBUG:
                #   print(" Group: Whitespace")
                result["whitespace"][char] += 1

    try:
        with open(filename) as f:
            # Progress will stop at 99% if this is 0
            # NOTE: might be fixed by a past change to update_progress_inline already.
            #       needs some testing
            progress = 1
            for line in f:
                analyze_line(line)
                update_progress_inline(
                    progress,
                    file_total_lines,
                    msg="Processing %s (%i lines)" % (filename, file_total_lines))
                progress += 1

        return result

    except Exception as err:
        # TODO: Handle all kinds of file exceptions
        raise err


def print_table(ordered_keys: list, data: dict, line_prefix='  '):
    """
    Print result dict in a fancy manner.

    Args:
        ordered_keys: The keys of 'data'. Data will be printed in the order of
            this list.
        data: Actual dict to be printed. Expected to have the same keys in
            ordered_keys and integers for values
        line_prefix: Optional prefix to be printed before each table line.
            A few spaces by default
    """

    # Width of the terminal
    cols, _ = shutil.get_terminal_size()
    # Current column
    count = 0
    # Max coloumns in a row
    columns = math.floor((cols - len(line_prefix)) / 20)
    # Has the prefix already been printed?
    prefixed = False
    for c in ordered_keys:
        if count == 0 and not prefixed:
            print(line_prefix, end='')
            prefixed = True

        if not PRESERVE_SPACE:
            # Printing chars with zero count is ok
            line = "%-6s: %-10s " % (repr(c), data[c])
            print(line, end='')

            count += 1
        elif not data[c] == 0:
            # Print only chars that have actually been found
            line = "%-6s: %-10s " % (repr(c), data[c])
            print(line, end='')

            count += 1

        # Print \n at the end of the row but only if there are items
        # left to print on the next row
        if count == columns and not ordered_keys.index(c) == len(ordered_keys) - 1:
            print()
            count = 0
            # New line needs its prefix
            prefixed = False


def print_results(result):
    """
    Print the results of the analysys.

    Args:
        result: Dict containing the character counts
    """
    locale.setlocale(locale.LC_ALL, locale.getdefaultlocale())

    # PRINT LOWERCASE LETTERS
    print("Lowercase letters ({0:n} total):".format(
        sum(result["lower"].values())))
    print_table(string.ascii_lowercase, result["lower"])
    print("\n")

    # PRINT UPPERCASE LETTERS
    print("Uppercase letters ({0:n} total):".format(
        sum(result["upper"].values())))
    print_table(string.ascii_uppercase, result["upper"])
    print("\n")

    # PRINT DIGITS
    print("Digits ({0:n} total):".format(sum(result["digits"].values())))
    print_table(string.digits, result["digits"])
    print("\n")

    # PRONT WHITESPACE
    print("Whitespaces ({0:n} total):".format(
        sum(result["whitespace"].values())))
    print_table(string.whitespace, result["whitespace"])
    print("\n")

    # PRINT PUNTUATION
    print("Punctiation/Special ({0:n} total):".format(
          sum(result["punctuation"].values())))
    print_table(string.punctuation, result["punctuation"])
    print("\n")


def main():
    """Entry point."""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        "filenames",
        help="One or more files that should be analyzed",
        nargs='+'
    )
    arg_parser.add_argument(
        "-e", "--extensive",
        help="Show all chars instead of only those found",
        action="store_false"
    )
    args = arg_parser.parse_args()

    global PRESERVE_SPACE
    PRESERVE_SPACE = args.extensive

    filenames = args.filenames

    result = {}
    result["lower"] = {c: 0 for c in string.ascii_lowercase}
    result["upper"] = {c: 0 for c in string.ascii_uppercase}
    result["digits"] = {c: 0 for c in string.digits}
    result["whitespace"] = {c: 0 for c in string.whitespace}
    result["punctuation"] = {c: 0 for c in string.punctuation}

    total_files = len(filenames)
    progress = 1
    for filename in filenames:
        result = count_file(
            filename,
            show_progress=True,
            use_result_var=result)
        print("Done! (%i/%i)" % (progress, total_files))
        progress += 1

    print("Analysys Complete!\n")
    print_results(result)

if __name__ == "__main__":
    main()
